import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


/**
 * Created by user on 09.01.17.
 */
public class WordCount {
    public static void main(String[] args) {

        SparkConf conf = new SparkConf();
        conf.setAppName("my spark");
        conf.setMaster("local[*]");

        JavaSparkContext sc = new JavaSparkContext(conf);

        WordCount wordCount = new WordCount();
        ClassLoader classLoader = wordCount.getClass().getClassLoader();
        JavaRDD<String> rdd = sc.textFile(classLoader.getResource("data.txt").getFile());
        //or
//        JavaRDD<String> rdd = sc.textFile("file:/home/user/IdeaProjects/spark/src/main/resources/data.txt");

        //sc.paralelalize from memory from arrray

        //length symbols
        //JavaRDD<Integer> lineLenghts = rdd.map(String::length);
        JavaRDD<Integer> lineLenghts = rdd.map(string -> string.split(" ").length);//слова
        int total = lineLenghts.reduce((a, b) -> a + b);
        System.out.println("totalLenghtSymbols = " + total);
        //length symbols

        JavaRDD<String> stringJavaRDD = sc.parallelize(Arrays.asList("groovy groovy maven maven scala Java jAva jaVa".split(" ")));
        List<String> top1 = topX(stringJavaRDD, 1);
        System.out.println("top1 = " + top1);

//        List<String> garbage = new ArrayList<>(Arrays.asList("two"));
//
//        List<String> list = new ArrayList<>(Arrays.asList("one two two three three".split(" ")));
//        list.stream()
//                .filter((word)->!garbage.contains(word))
//                .map(String::toUpperCase)
//                .forEach(System.out::println);

        List<String> top2 = topX(rdd, 3);
        System.out.println("top2 = " + top2);


    }

    public static List<String> topX(JavaRDD<String> lines, int x) {
        List<String> garbage = new ArrayList<>(Arrays.asList("a we in".split(" ")));


        return lines
                .flatMap(
                        new FlatMapFunction<String, String>() {
                            @Override
                            public Iterator<String> call(String line) {
                                return Arrays.asList(line.toLowerCase().split(" ")).iterator();
                            }

                        }
                )
                .filter(word -> !garbage.contains(word))
                .mapToPair(word -> new Tuple2<>(word, 1))
                .reduceByKey((a, b) -> a + b)
                .mapToPair(Tuple2::swap)
                .sortByKey(false)
                .map(Tuple2::_2)
                .take(x);
    }
}

/*
Spark Context local and production
Spring profile

@Bean
@Profile("LOCAL")
public JavaSparkContext sc(){
    SparkConf conf=new SparkConf();
    conf.setAppName("music analyze");
    conf.setMaster("local[1]");
     return new JavaSparkContext(conf);
}

@Bean
@Profile("PROD")
public JavaSparkContext sc(){
    SparkConf conf=new SparkConf();
    conf.setAppName("music analyze");
    return new JavaSparkContext(conf);
}

похоже на java 8 streams

transformations: return rdd
map
flatMap
filter
mapPartitions mapPartitionsWithIndex
sample
union  intersection join cogroup cartesian(otherDataset)
distinct
reduceByKey aggregateByKey sortByKey
pipe
coalesce repartition repartitionAndSortWithinPartitions

action:
return for driver:
reduce
collect
count countByKey countByValue
first
take takeSample takeOrdered
foreach

save for cluster
saveAsTextFile saveAsSequenceFile saveAsObjectFile




(LAZY)rdd Flow ОБРАБОТКА ЗАПУСКАЕТСЯ ПОСЛЕ ACTION
DATE -> rdd1(transformation)-rdd2(transformation)-rdd3(action)->BENEFIT
НО если нам надо больше одного action (вилка)
когда делаем коллект то все сотни гигов с разных класстеров идут на машину драйвер (где запустилось)
поэтому делаем sc.persist(StorageLevel.memoryOnly2 (сохранение на двух машинах)) для сохранения промежуточной обработки на класстере
persist not action this is (marker for action)
=============
@Service which use rdd must be serializable
----------
shared data for workers
1) with out broadcast

2)with broadcast !not autowired for task! если информация нужна всем то
  @AutowiredBroadCast
  private Broadcast<UserConfig> broadcastUserConfig;//.value.garbage

  -------------

  Use dataframes function example
  SparkContext(sparkconf);
  SQLContext(sparkContext)
  DataFrame frame = sqlContext.read().json("data/json");
  frame.show();

  DataFrame keywords = frame.select(functions.explode(functions.column("keywords").as("keyword")));
  DataFrame orderedBy  = keywords.groupBy("keyword")
                        .agg(functions.count("keyword").as("amount"))
                        .orderBy(functions.column("amount").desc());
  orderedBy.show();
  String mostPopular = orderBy.first().getString(0);

  -------
  RowFactory return JavaRDD<Row>
  DataFrame = sqlContext.createDataFrame(rdd(rows), DataTypes.createStructType(new StructField[]{
    DataTypes.createStructField("words", DataTypes.StringType, true)
  }))

  create our function for dataFrame use udf (create - register - use)

*/
